FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY ./app /app
WORKDIR /app
EXPOSE 5000