from fastapi import FastAPI, Request, Depends, Response, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from authlib.integrations.starlette_client import OAuth
from starlette.config import Config
from starlette.responses import HTMLResponse, RedirectResponse, FileResponse
from starlette.middleware.sessions import SessionMiddleware
from authlib.integrations.starlette_client import OAuth, OAuthError
import typing as t
import csv
import datetime
import logging

logger = logging.getLogger(__name__)

config = Config('.env')
oauth = OAuth(config)
oauth.register(
    name='cern',
    server_metadata_url='https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration',
    client_kwargs={
        'scope': 'openid email'
    },
    client_id=config('client_id'),
    client_secret=config('client_secret')
)


class ScanItem(BaseModel):
    code: str


class RequiresLoginException(Exception):
    pass


async def redirect() -> bool:
    raise RequiresLoginException


def get_user(request: Request):
    if 'user' not in request.session:
        raise RequiresLoginException
    return request.session.get('user')


def parse_scanitem(scanitem: ScanItem):
    import json
    data = json.loads(scanitem.code)
    item, action = data.get('item'), data.get('action')
    return item, action


app = FastAPI()
app.mount('/static', StaticFiles(directory='static'), name='static')
app.add_middleware(SessionMiddleware, secret_key='my-secret-key')
templates = Jinja2Templates(directory="templates")


@app.exception_handler(RequiresLoginException)
async def exception_handler(request: Request, exc: RequiresLoginException) -> Response:
    return RedirectResponse(url='/login')


@app.route('/login')
async def login(request: Request):
    # absolute url for callback
    # we will define it below
    redirect_uri = request.url_for('auth')
    print(oauth.config('client_id'))
    return await oauth.cern.authorize_redirect(request, redirect_uri)


@app.route('/auth')
async def auth(request: Request):
    try:
        token = await oauth.cern.authorize_access_token(request)
    except OAuthError as error:
        return HTMLResponse(f'<h1>{error.error}</h1>')
    resource = await oauth.cern.parse_id_token(request, token)
    request.session['user'] = resource['cern_upn']
    return RedirectResponse(url='/')


@app.route('/download')
async def download(request: Request, user: str = Depends(get_user)):
    try:
        return FileResponse(config('filepath'), headers={'Content-Disposition': 'attachment;filename=data.csv'})
    except Exception as e:
        raise HTTPException(status_code, detail=f"Error in download file: {e}")


@app.post('/data')
async def data(scanitem: ScanItem, user: str = Depends(get_user)):
    timestamp = datetime.datetime.utcnow().isoformat()
    try:
        item, action = parse_scanitem(scanitem)
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="Wrong parsing of the code: " + str(e))
    if not item or not action:
        raise HTTPException(status_code=400, detail="Item or action not valid")
    try:
        with open(config("filepath"), "a+") as f:
            writer = csv.writer(f)
            writer.writerow([timestamp, user, item, action])

        data = {"item": item, "action": action,
                "user": user, "timestamp": timestamp}
        logger.info(f"Data written to csv: {data}")
        return data
    except Exception as e:
        raise HTTPException(
            status_code=500, detail="File writing went wrong: " + str(e))


@app.get('/')
async def root(request: Request, user: str = Depends(get_user)):
    return templates.TemplateResponse("index.html", {'request': request, 'user': user})
